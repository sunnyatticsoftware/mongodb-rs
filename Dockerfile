FROM ubuntu:20.04

RUN apt-get update && apt-get install -y ca-certificates curl gnupg
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
ENV NODE_MAJOR 20
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update && apt-get install -y nodejs

RUN node --version

# Install application
WORKDIR /app
ENV MONGOMS_DEBUG=0
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 27017
HEALTHCHECK --interval=30s --timeout=3s --retries=3 \
  CMD [ "echo", "OK" ]

CMD ["node", "start-replica-set.js"]
