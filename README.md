# mongodb-rs

MongoDB Replica Set docker image for testing purposes. It has the following features:

- 1 node replica set
- Change Streams support
- In-memory persistence (data is erased when stopping container)

Run with

```
docker run --name mongodbrs -p 27017:27017 registry.gitlab.com/sunnyatticsoftware/mongodb-rs
```

Optionally, pass an environment variable `MONGOMS_DEBUG` to enable debug

```
docker run --name mongodbrs -p 27017:27017 -e MONGOMS_DEBUG=1 registry.gitlab.com/sunnyatticsoftware/mongodb-rs
```

And connect to connection string

```
mongodb://127.0.0.1:27017/?replicaSet=rs0
```
