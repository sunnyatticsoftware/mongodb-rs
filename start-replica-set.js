import { MongoMemoryReplSet } from "mongodb-memory-server";

console.log(
  `Initializing Mongo Memory Replica Set with DEBUG ${
    process.env.MONGOMS_DEBUG === "1" ? "enabled" : "disabled"
  }`
);

// This will create an new instance of "MongoMemoryReplSet" and automatically start all Servers
const replset = await MongoMemoryReplSet.create({
  instanceOpts: [
    {
      port: 27017,
      storageEngine: "wiredTiger"
    }
    // each entry will result in a MongoMemoryServer (replSet.count will not count towards here)
  ],
  replSet: {
    name: "rs0",
    ip: `::,0.0.0.0`,
    count: 1,
    storageEngine: "wiredTiger" // by default was ephermeralForTest
  }
});

console.log("Replica set is running...");
const uri = replset.getUri();
console.log(`Connect using: ${uri}`);

// The ReplSet can be stopped again with
//await replset.stop();

// Listen for termination signals
process.on("SIGINT", gracefulShutdown);
process.on("SIGTERM", gracefulShutdown);

function gracefulShutdown() {
  console.log("\nReceived shutdown signal. Stopping replica set...");
  replset.stop().then(() => {
    console.log("Replica set stopped. Exiting...");
    process.exit(0);
  });
}
